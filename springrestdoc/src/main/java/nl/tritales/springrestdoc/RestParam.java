/*
 * Copyright 2014-2015 Wim van Haaren (wim@tritales.nl) All Rights Reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tritales.springrestdoc;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public class RestParam {

    // private final String paramString;
    private final String annotation;
    private final Class<?> parameterType;
    private final String parameterName;
    private String defaultValue;
    private String required;
    private String dateTimeFormat;

    public RestParam(final PathVariable pv, final Class<?> parameterType) {
        this.annotation = "@PathVariable";
        this.parameterType = parameterType;
        this.parameterName = pv.value();
    }

    public RestParam(final RequestParam rp, final Class<?> parameterType, final String defaultValue) {
        this.annotation = "@RequestParam";
        this.parameterType = parameterType;
        if (StringUtils.isNotBlank(defaultValue)) {
            this.defaultValue = defaultValue;
        }
        this.parameterName = rp.value();
        if (rp.required()) {
            this.required = "required";
        }
    }

    public RestParam(final RequestBody rb, final MethodParameter param) {
        this.annotation = "@RequestBody";
        this.parameterType = param.getParameterType();
        this.parameterName = param.getParameterName();
        if (rb.required()) {
            this.required = "required";
        }
    }

    public String getAnnotation() {
        return annotation;
    }

    public Class<?> getParameterType() {
        return parameterType;
    }

    public String getParameterName() {
        return parameterName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getRequired() {
        return required;
    }

    public String getDateTimeFormat() {
        return dateTimeFormat;
    }

    public void setDateTimeFormat(final DateTimeFormat dateTimeFormat) {
        if (dateTimeFormat != null) {
            this.dateTimeFormat = "@DateTimeFormat(iso=" + dateTimeFormat.iso() + ", pattern=" + dateTimeFormat.pattern() + ", style=" + dateTimeFormat.style() + ")";
        } else {
            this.dateTimeFormat = null;
        }
    }

}
