/*
 * Copyright 2014-2015 Wim van Haaren (wim@tritales.nl) All Rights Reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tritales.springrestdoc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ValueConstants;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaSource;
import com.thoughtworks.qdox.model.JavaType;

public class SpringRestDoc {
    private final SortedSet<Resource> resources = new TreeSet<>();

    private final MultiMap<Resource, Entry<RequestMappingInfo, HandlerMethod>> resourceHandlerMethodsEntryMap =
            new MultiValueMap<Resource, Entry<RequestMappingInfo, HandlerMethod>>();

    private final Map<RequestMappingInfo, HandlerMethod> handlerMethods;

    private File sourceDir;

    public SpringRestDoc(final Map<RequestMappingInfo, HandlerMethod> handlerMethods) {
        this(handlerMethods, null);
    }

    public SpringRestDoc(final Map<RequestMappingInfo, HandlerMethod> handlerMethods, final File sourceDir) {
        this.handlerMethods = handlerMethods;
        if (sourceDir != null && sourceDir.isDirectory()) {
            this.sourceDir = sourceDir;
        }
        // Step 1. Get all resources
        findAllResources();

        // Step 2. For each resource, get all methods
        findAllMethods();

        // Step 3. Generate some output (asciidoc or json, or whatever...)
        // for now some simple json with sysout
    }

    public void asciidoc(final Writer writer) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("== Resources\n\n");
        for (final Resource resource : resources) {
            sb.append("\n=== " + resource.getName() + "\n\n");
            sb.append(resource.getDescription() == null ? "" : resource.getDescription() + "\n\n");
            sb.append("*URI*: `" + resource.getUri() + "` +\n");
            sb.append("*@RestController*: `" + resource.getRestController() + "`\n\n");
            for (final Method method : resource.getMethods()) {
                String verbs = "";
                for (final RequestMethod rm : method.getVerbs()) {
                    verbs += rm.name() + " ";
                }
                String uris = "";
                final String[] methodUris = method.getUris().toArray(new String[method.getUris().size()]);
                for (int i = 0; i < methodUris.length; i++) {
                    uris += methodUris[i];
                    if (i < (methodUris.length - 1)) {
                        uris += " | ";
                    }
                }
                sb.append("\n===== " + verbs + uris + "\n");
                if (StringUtils.isNotBlank(method.getDescription())) {
                    sb.append("\n" + method.getDescription() + "\n\n");
                }
                consumesAsciidoc(sb, method);
                paramsAsciidoc(sb, method);
                producesAsciidoc(sb, method);
                sb.append("\n*returns*: " + method.getReturns() + "\n");
                sb.append("\n'''");
                sb.append("\n");
            }
            sb.append("\n");
            sb.append("<<<");
            sb.append("\n");
        }
        writer.append(sb.toString());
        writer.flush();
        writer.close();
    }

    private void consumesAsciidoc(final StringBuilder sb, final Method method) {
        sb.append("*consumes*: ");
        for (final String consumes : method.getConsumes()) {
            sb.append(consumes + " ");
        }
        sb.append(" \n");
    }

    private void paramsAsciidoc(final StringBuilder sb, final Method method) {
        final List<RestParam> parameters = method.getParameters();
        if (!parameters.isEmpty()) {
            sb.append("\nparameters: ::\n");
            for (int i = 0; i < parameters.size(); i++) {
                final RestParam param = parameters.get(i);
                sb.append("* " + param.getAnnotation() + " ");
                if (param.getDateTimeFormat() != null) {
                    sb.append(param.getDateTimeFormat() + " ");
                }
                sb.append(param.getParameterType().getCanonicalName() + " ");
                sb.append(param.getParameterName() == null ? "" : param.getParameterName() + " ");
                sb.append(param.getDefaultValue() == null ? "" : "defaultValue=\"" + param.getDefaultValue() + "\" ");
                sb.append(param.getRequired() == null ? "" : "required");
                sb.append(" \n");
            }
            sb.append(" \n");
        }
    }

    private void producesAsciidoc(final StringBuilder sb, final Method method) {
        sb.append("*produces*: ");
        for (final String produces : method.getProduces()) {
            sb.append(produces + " ");
        }
        sb.append(" \n");
        final String defaultHttpStatus = method.getDefaultHttpStatus();
        if (defaultHttpStatus != null) {
            sb.append("\n*default HTTP response status*: " + defaultHttpStatus + " \n");
        }
    }

    private void findAllResources() {
        // Loop over all mapped classes...
        for (final Entry<RequestMappingInfo, HandlerMethod> entry : handlerMethods.entrySet()) {
            final HandlerMethod value = entry.getValue();
            final Class<?> declaringClass = value.getMethod().getDeclaringClass();
            if (!declaringClass.isAnnotationPresent(RestController.class)) {
                // Only @RestControllers allowed...
                break;
            }

            // Get the URI...
            final RequestMapping classRequestMapping = declaringClass.getAnnotation(RequestMapping.class);
            String uri = null;
            if (classRequestMapping != null) {
                uri = classRequestMapping.value()[0];
            }

            final Resource resource = new Resource(uri, declaringClass, sourceDir);
            resourceHandlerMethodsEntryMap.put(resource, entry);
            resources.add(resource);
        }
    }

    @SuppressWarnings("unchecked")
    private void findAllMethods() {
        for (final Resource resource : resources) {
            // Get the mapping info and handler methods that are mapped to this
            // resource...
            final Collection<Entry<RequestMappingInfo, HandlerMethod>> entries = (Collection<Entry<RequestMappingInfo, HandlerMethod>>) resourceHandlerMethodsEntryMap.get(resource);
            for (final Entry<RequestMappingInfo, HandlerMethod> entry : entries) {
                final Set<RequestMethod> requestMethods = entry.getKey().getMethodsCondition().getMethods(); // GET,
                // POST,
                // etc.
                final Set<String> uriPatterns = entry.getKey().getPatternsCondition().getPatterns();
                final Set<MediaType> consumableMediaTypes = entry.getKey().getConsumesCondition()
                        .getConsumableMediaTypes();
                final Set<MediaType> producibleMediaTypes = entry.getKey().getProducesCondition()
                        .getProducibleMediaTypes();
                final List<RestParam> restParameters = params(entry);

                final HttpStatus defaultHttpStatus = determineDefaultHttpStatus(entry);

                String returns = "";
                final Class<?> returnType = entry.getValue().getReturnType().getMethod().getReturnType();
                if (Void.TYPE.equals(returnType)) {
                    returns = "void";
                } else if (returnType.isAssignableFrom(ResponseEntity.class)) {
                    final Type genericReturnType = entry.getValue().getMethod().getGenericReturnType();
                    returns = genericReturnType.toString();
                } else {
                    returns = returnType.getCanonicalName();
                }
                final String description = descriptionFromJDoc(entry.getValue());
                final Method method = new Method(description, requestMethods, uriPatterns, consumableMediaTypes,
                        producibleMediaTypes, defaultHttpStatus, restParameters, returns);
                resource.add(method);
            }
        }
    }

    private HttpStatus determineDefaultHttpStatus(final Entry<RequestMappingInfo, HandlerMethod> entry) {
        HttpStatus defaultHttpStatus = null;
        final Annotation[] anns = entry.getValue().getReturnType().getMethod().getAnnotations();
        for (final Annotation ann : anns) {
            if (ann.annotationType().isAssignableFrom(ResponseStatus.class)) {
                defaultHttpStatus = ((ResponseStatus)ann).value();
                break;
            }
        }
        return defaultHttpStatus;
    }

    private List<RestParam> params(final Entry<RequestMappingInfo, HandlerMethod> entry) {
        final List<RestParam> params = new ArrayList<>();

        final HandlerMethod handlerMethod = entry.getValue();
        for (final MethodParameter param : handlerMethod.getMethodParameters()) {
            RestParam restParam = null;
            DateTimeFormat dateTimeFormatAnnotation = null;
            final Annotation[] annotations = param.getParameterAnnotations();
            for (final Annotation ann : annotations) {
                // @PathVariable
                if (ann.annotationType().isAssignableFrom(PathVariable.class)) {
                    final PathVariable pv = (PathVariable) ann;
                    restParam = new RestParam(pv, param.getParameterType());
                }
                // @RequestParam
                else if (ann.annotationType().isAssignableFrom(RequestParam.class)) {
                    final RequestParam rp = (RequestParam) ann;
                    String defaultValue = "";
                    if (!ValueConstants.DEFAULT_NONE.equalsIgnoreCase(rp.defaultValue())) {
                        defaultValue = rp.defaultValue();
                    }
                    restParam = new RestParam(rp, param.getParameterType(), defaultValue);
                }
                // @RequestBody
                else if (ann.annotationType().isAssignableFrom(RequestBody.class)) {
                    final RequestBody rb = (RequestBody) ann;
                    restParam = new RestParam(rb, param);
                }

                // Is there a @DateTimeFormat?
                if (ann.annotationType().isAssignableFrom(DateTimeFormat.class)) {
                    dateTimeFormatAnnotation = (DateTimeFormat) ann;
                }
            }
            params.add(restParam);

            if (restParam != null && dateTimeFormatAnnotation != null) {
                restParam.setDateTimeFormat(dateTimeFormatAnnotation);
            }
        }

        return params;
    }

    private String descriptionFromJDoc(final HandlerMethod handlerMethod) {
        if (this.sourceDir == null) {
            return null;
        }
        final Class<?> declaringClass = handlerMethod.getBeanType();
        try {
            final JavaProjectBuilder builder = new JavaProjectBuilder();
            final File file = new File(sourceDir, declaringClass.getCanonicalName().replace(".", "/") + ".java");
            final JavaSource src = builder.addSource(file);
            final JavaClass jc = src.getClasses().get(0);
            final List<JavaMethod> methods = jc.getMethods();
            for (final JavaMethod m : methods) {
                if (sameSignature(handlerMethod, m)) {
                    return m.getComment();
                }
            }
        } catch (final FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    private boolean sameSignature(final HandlerMethod method, final JavaMethod m) {
        if (method.getMethod().getName().equals(m.getName())) {
            final MethodParameter[] methodParameters = method.getMethodParameters();
            final List<JavaType> mParameterTypes = m.getParameterTypes();
            if (methodParameters.length != mParameterTypes.size()) {
                return false;
            }
            for (int i = 0; i < methodParameters.length; i++) {
                final MethodParameter param1 = methodParameters[i];
                final JavaType param2 = mParameterTypes.get(i);
                final Class<?> type1 = param1.getParameterType();
                final String s1 = type1.getCanonicalName();
                final String s2 = param2.getCanonicalName();
                if (!s1.equals(s2)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public String json() throws JsonProcessingException {
        return this.json(false);
    }

    public String json(final boolean prettyPrint) throws JsonProcessingException {
        final ObjectMapper mapper = new ObjectMapper();
        if (prettyPrint) {
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
        }
        mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper.writeValueAsString(resources);
    }

}
