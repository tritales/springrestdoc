# SpringRestDoc #

**SpringRestDoc**  is a tool to have your RESTful API documentation automatically generated for you for all your `@RestController` classes.

Unlike tools like Swagger, **SpringRestDoc** does not require you to pollute your code with custom annotations. Instead it tries to find out what it can for any class annotated with a `@RestController` annotation. It does this by levaraging Spring's MockMVC test support. Just make sure to initialize MockMvc through its `webAppContextSetup()` and not its `standaloneSetup()`. Also SpringRestDoc requires a reference to `RequestMappingHandlerMapping` which you can have Spring autowire into your test class.

Optionally, javadoc can be used to generate a summary or description of the resource and/or methods. We chose this way instead of a custom annotation, for example, because we don't wont to pollute (y)our code with custom annotations. Works at class- and method-level javadoc. All you have to do - besides write the actual javadoc comments ;) - is tell SpringRestDoc where your source files are located.

## How do I get set up? ##

First, include SpringRestDoc as a test dependency in your project. SpringRestDoc has been published to the Maven central repository.
You can add it by adding the following lines to your pom.xml :


```
#!xml

<dependency>
    <groupId>nl.tritales.springrestdoc</groupId>
    <artifactId>springrestdoc</artifactId>
    <version>0.1.0</version>
    <scope>test</scope>
</dependency>
```


Write a unit test using MockMvc:


```
#!java

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("/test-context.xml")
public class SpringRestDocumentationTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() throws Exception {
        final Map<RequestMappingInfo, HandlerMethod> handlerMethods = handlerMapping.getHandlerMethods();
        final SpringRestDoc springRestDoc = new SpringRestDoc(handlerMethods);
        System.out.println(springRestDoc.json(true));
    }

}
```

### Description through javadoc ###

Optionally, you can include the location of the source files. This will result in Javadoc comment getting included in the output as a `"description"` field:


```
#!java

    @Test
    public void test() throws Exception {
        final Map<RequestMappingInfo, HandlerMethod> handlerMethods = handlerMapping.getHandlerMethods();
        final String sourceFolder = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "java";
        final SpringRestDoc springRestDoc = new SpringRestDoc(handlerMethods, new File(sourceFolder));
        System.out.println(springRestDoc.json(true));
    }

```


### asciidoc output ###

Besides json it is also possible to generate some asciidoc output:


```
#!java

    @Test
    public void test() throws Exception {
        final Map<RequestMappingInfo, HandlerMethod> handlerMethods = handlerMapping.getHandlerMethods();
        final String sourceFolder = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "java";
        final SpringRestDoc springRestDoc = new SpringRestDoc(handlerMethods, new File(sourceFolder));

        final File outputFolder = new File(System.getProperty("user.dir") + File.separator + "target" + File.separator + "generated-doc");
        outputFolder.mkdirs();
        final FileWriter writer = new FileWriter(new File(outputFolder, "resources.asciidoc"));
        springRestDoc.asciidoc(writer);
    }

```

And then you probably also want to include the asciidocter-plugin (if you're using maven, that is):


```
#!xml

<plugin>
	<groupId>org.asciidoctor</groupId>
	<artifactId>asciidoctor-maven-plugin</artifactId>
	<version>1.5.2</version>
	<executions>
		<execution>
			<id>generate-docs</id>
			<phase>package</phase>
			<goals>
				<goal>process-asciidoc</goal>
			</goals>
			<configuration>
				<backend>html</backend>
				<doctype>book</doctype>
				<!-- sourceDirectory>${project.build.directory}/generated-doc</sourceDirectory -->
				<attributes>
					<generated>${project.build.directory}/generated-doc</generated>
				</attributes>
			</configuration>
		</execution>
	</executions>
</plugin>
```




### Release notes ###

#### New in version 0.1.1-SNAPSHOT ####

* Support for @DateTimeFormat
* Support for @ResponseStatus

### TODO's ###

At this moment the project is a very rough sketch of what it could be.
Still to do:

 * provide a sample project that shows how to combine the generated documentation with hand written documumentation (using asciidoc)
 * @Description
 * How to show Resources? Can we serialize an object to json? Or can we show a sample request-response?
 * Make some assumptions configurable (e.g., default MediaType which now is "application/json")
 * documentation
 * WADL output
 * How to document dependencies on other services?
 * Auto-register services with a REST registry (appllication listener, SWIL, ...)
 * and much, much more...



# How can I help #

There are many ways in which help is appreciated. You can use SpringRestDoc and report any issues you might encounter. If there is some functionality you might find missing, please file a request for it. Any contributions to the source code are of course more than welcome.

Besides the above, any donations are very welcome. [Donate with PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=5MA2YM9KQSD8A&lc=US&item_name=SpringRestDoc&item_number=springrestdoc&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)
